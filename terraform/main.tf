terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = "0.70.0"
    }
  }
}

provider "yandex" {
  token     = chomp(file("${path.module}/oauth"))
  cloud_id  = "b1gj7lbrq4k3dons5h1k"
  folder_id = "b1g3241pil19n1gsbkor"
  zone      = "ru-central1-a"
}

data "yandex_compute_image" "container-optimized-image" {
  family = "container-optimized-image"
}

resource "yandex_compute_instance" "metasearch" {
  name        = "metasearch"
  platform_id = "standard-v2"
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.container-optimized-image.id
    }
  }
  network_interface {
    subnet_id = "e9b3kphk6722n5l8adth"
    nat = true
  }
  resources {
    cores         = 2
    memory        = 1
    core_fraction = 5
  }
  metadata = {
    user-data = file("${path.module}/cloud_config.yaml")
  }
}

resource "yandex_compute_instance" "search-in-shards" {
  name        = "search-in-shards"
  platform_id = "standard-v2"
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.container-optimized-image.id
    }
  }
  network_interface {
    subnet_id = "e9b3kphk6722n5l8adth"
    nat = true
  }
  resources {
    cores         = 2
    memory        = 1
    core_fraction = 5
  }
  metadata = {
    user-data = file("${path.module}/cloud_config.yaml")
  }
}

resource "yandex_compute_instance" "user-service" {
  name        = "user-service"
  platform_id = "standard-v2"
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.container-optimized-image.id
    }
  }
  network_interface {
    subnet_id = "e9b3kphk6722n5l8adth"
    nat = true
  }
  resources {
    cores         = 2
    memory        = 1
    core_fraction = 5
  }
  metadata = {
    user-data = file("${path.module}/cloud_config.yaml")
  }
}

resource "yandex_compute_instance" "shard0" {
  name        = "shard0"
  platform_id = "standard-v2"
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.container-optimized-image.id
    }
  }
  network_interface {
    subnet_id = "e9b3kphk6722n5l8adth"
    nat = true
  }
  resources {
    cores         = 2
    memory        = 1
    core_fraction = 5
  }
  metadata = {
    user-data = file("${path.module}/cloud_config.yaml")
  }
}

resource "yandex_compute_instance" "shard1" {
  name        = "shard1"
  platform_id = "standard-v2"
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.container-optimized-image.id
    }
  }
  network_interface {
    subnet_id = "e9b3kphk6722n5l8adth"
    nat = true
  }
  resources {
    cores         = 2
    memory        = 1
    core_fraction = 5
  }
  metadata = {
    user-data = file("${path.module}/cloud_config.yaml")
  }
}
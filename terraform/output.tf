output "external_ip_metasearch" {
  value = yandex_compute_instance.metasearch.network_interface.0.nat_ip_address
}

output "external_ip_searchinshards" {
  value = yandex_compute_instance.search-in-shards.network_interface.0.nat_ip_address
}

output "external_ip_users" {
  value = yandex_compute_instance.user-service.network_interface.0.nat_ip_address
}

output "external_ip_shard0" {
  value = yandex_compute_instance.shard0.network_interface.0.nat_ip_address
}

output "external_ip_shard1" {
  value = yandex_compute_instance.shard1.network_interface.0.nat_ip_address
}
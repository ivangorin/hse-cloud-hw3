FROM python:3.9

RUN mkdir /search_app/
WORKDIR /search_app/

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY .coveragerc .coveragerc

# COPY data data
COPY src src

ENV PYTHONPATH=$PYTHONPATH:/search_app/src/search

# CMD ["python", "src/search/simpleSearch/main.py"]
WORKDIR /search_app/src/search/simpleSearch/
CMD ["gunicorn", "--bind=0.0.0.0:8000", "--workers=2", "main:main()"]
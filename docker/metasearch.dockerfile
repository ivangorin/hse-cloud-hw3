FROM python:3.9

RUN mkdir /search_app/
WORKDIR /search_app/

COPY requirements.txt .
RUN pip install -r requirements.txt
COPY .coveragerc .coveragerc

COPY src src

# CMD ["python", "src/search/metasearch/main.py"]
WORKDIR /search_app/src/search/metasearch/
CMD ["gunicorn", "--bind=0.0.0.0:8000", "--workers=2", "main:main()"]
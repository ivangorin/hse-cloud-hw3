from flask import Flask, request
from prometheus_flask_exporter import PrometheusMetrics
from time import sleep
from random import random

class Server(Flask):
    def __init__(self, name: str):
        super().__init__(name)
        urls = [
            ('/search', self.search, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        sleep(0.1)
        return {'search_results': 'none'}

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)


server = Server('server_name')
metrics = PrometheusMetrics(server)
metrics.info('app_info', 'Application info', version='1.0.3')

server.run_server()
from common.data_source import CSV
from server import Server
from service import UserService
import os


def main():
    USER_DATA_FILE = os.getenv('USER_DATA_FILE')
    user_service = UserService(CSV(USER_DATA_FILE))
    server = Server('user', user_service=user_service)
    # server.run_server(debug=True)
    return server


if __name__ == '__main__':
    main()

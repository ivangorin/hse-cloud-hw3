from server import Server
from service import SearchInShardsService
import os

def main():
    SHARD_HOSTS = os.getenv('SHARD_HOSTS').split(',')
    SHARD_PORTS = os.getenv('SHARD_PORTS')
    if SHARD_PORTS is None:
        SHARD_PORTS = ['8000' for _ in range(len(SHARD_HOSTS))]
    search = SearchInShardsService(
        shard_hosts=SHARD_HOSTS,
         shard_ports=SHARD_PORTS
    )
    server = Server('searchInShards', service=search)
    # server.run_server(debug=True)
    return server


if __name__ == '__main__':
    main()

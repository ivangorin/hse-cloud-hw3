from simpleSearch.service import SimpleSearchService
from typing import List
import pandas as pd
import requests


class SearchInShardsService(SimpleSearchService):
    def __init__(self, shard_hosts: List[str], shard_ports: List[str]):
        assert len(shard_hosts) == len(shard_ports)
        self._shards = [shard_hosts[i] + ':' + shard_ports[i] for i in range(len(shard_hosts))]

    def get_search_data(self, search_text, user_data, limit) -> pd.DataFrame:
        shards_responses = []
        for shard in self._shards:
            resp = requests.get(
                f'http://{shard}/search',
                params={
                    'search_text': search_text,
                    'limit': limit
                },
                json=user_data
            )
            shards_responses.append(pd.DataFrame(resp.json()['search_results']))
        self._data = pd.concat(shards_responses)  # possible data race in case of multi thread/async usage
        self._data.reset_index(inplace=True, drop=True)
        assert self._data.index.is_unique
        return super().get_search_data(search_text, user_data, limit)

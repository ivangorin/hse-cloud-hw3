from typing import List, Dict
import requests


class MetaSearchService:
    def __init__(self, search_host: str, user_host: str, search_port: str, user_port: str) -> None:
        self._search_host = search_host
        self._search_port = search_port
        self._user_host = user_host
        self._user_port = user_port

    def search(self, search_text, user_id, limit=10) -> List[Dict]:
        user_data = requests.get(
            f'http://{self._user_host}:{self._user_port}/users',
            params={'user_id': user_id}
        ).json()
        search_result = requests.get(
            f'http://{self._search_host}:{self._search_port}/search',
            params={'search_text': search_text, 'limit': limit},
            json=user_data
        ).json()
        return search_result['search_results']

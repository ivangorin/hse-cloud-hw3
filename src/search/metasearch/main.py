from service import MetaSearchService
from server import Server
from prometheus_flask_exporter import PrometheusMetrics
import os

def main():
    SEARCH_HOST = os.getenv('SEARCH_HOST')
    SEARCH_PORT = os.getenv('SEARCH_PORT', '8000')
    USER_HOST = os.getenv('USER_HOST')
    USER_PORT = os.getenv('USER_PORT', '8000')
    metasearch = MetaSearchService(SEARCH_HOST, USER_HOST, SEARCH_PORT, USER_PORT)
    server = Server('metasearch', metasearch=metasearch)
    metrics = PrometheusMetrics(server)
    
    # server.run_server(debug=True)
    return server


if __name__ == '__main__':
    main()

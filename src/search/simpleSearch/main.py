from common.data_source import CSV
from server import Server
from service import SimpleSearchService
import os

def main():
    DATA_DIR = os.getenv("DATA_DIR")
    SHARD_ID = int(os.getenv("SHARD_ID"))
    SHARD_DATA_FILE = os.path.join(DATA_DIR, f'news_generated.{SHARD_ID}.csv')
    search = SimpleSearchService(CSV(SHARD_DATA_FILE))
    server = Server('simpleSearch', service=search)
    # server.run_server(debug=True)
    return server


if __name__ == '__main__':
    main()

from flask import Flask, request

from service import SimpleSearchService


class Server(Flask):
    def __init__(self, name: str, service: SimpleSearchService):
        super().__init__(name)
        self._search_service = service
        urls = [
            ('/search', self.search, {}),
        ]
        for url in urls:
            if len(url) == 3:
                self.add_url_rule(url[0], url[1].__name__, url[1], **url[2])

    def search(self):
        search_text = request.args.get('search_text')
        limit = int(request.args.get('limit'))
        user_data = request.json
        sr = self._search_service.get_search_data(search_text, user_data, limit)
        return {'search_results': sr.to_dict('records')}

    def run_server(self, **kwargs):
        super().run(host='0.0.0.0', port=8000, **kwargs)


# Shard

`curl --header "Content-Type: application/json" --request GET --data '{"age":40,"gender":"female"}' "localhost:8000/search?search_text=test&limit=10"`

# gunicorn

`SHARD_ID=1 DATA_DIR=~/cloudcomputing/3hw/hse-cloud-hw3/data/ gunicorn -w 6 --pythonpath ~/cloudcomputing/3hw/hse-cloud-hw3/src/search "main:main()"`

